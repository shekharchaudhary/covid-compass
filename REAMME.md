# COVID-19 Tracker

## Project Overview

This COVID-19 Tracker is SPA a web application developed using Create React App and Bootstrap. It's designed to provide real-time data on COVID-19 cases globally, including statistics on confirmed cases, recoveries, hospitilization and deaths. For visual use and for filtering data i have used line chart to show the data for all U.S and also by States.

## Application navigation (how it works)

### Navigation and Data Selection
Users can navigate between two main views: "All U.S. Data by Date" and "States / Territory Data". There is also a dropdown menu for selecting specific U.S. states, allowing users to view data at a more granular level.

### Data Visualization:

Initial State of the application shows a line chart that plots historical data of tests, cases, hospitalizations, and deaths over time for the entire United States. Each metric is color-coded for clear differentiation. You can interact with graph to see the more specific cases in paercular date. For filtering data i have used levels on top, which you can click to cross out and see the granular data for other category.

secong when you click toggle, than next state of app is presented, wher you can see Pie chart with the current COVID-19 data as of a specific date (March 7, 2021).This visualization provides a quick breakdown of the total number of cases, deaths, hospitalizations, and tests conducted.

And last state shows also line chart chart similar to the first one but focuses on a specific state (California). It provides a historical view of COVID-19 data points for that state. Additionally you can she a Input form to which is auto complete input form where you can choose specific state / territory (56)  to see the date of the each states via chart which for individual states or territory more granular way. 

### User Interface 
The interface is straightforward, with a clean and minimal design. It uses tabs and dropdowns for user interaction and charts for displaying data.

### Functionality 
You can likely interact with the charts to get more detailed information. For example, hovering over the line chart could display precise numbers for a given date.


The app could be a useful tool for public health officials, researchers, and the general public to track the progression of the COVID-19 pandemic over time. It provides historical data trends and current snapshots, which are essential for understanding the state of the pandemic and making informed decisions.

## Features
- **Real-Time Data**: Fetches the latest COVID-19 statistics from The COVID Tracking Project (https://covidtracking.com/data/api/version-2 )Project at The Atlantic API.
- **Coverage**: Provides data from the U. S.
- **Line chart and Pie**: Visualizes the spread and impact of the virus on a global map.
- **Responsive Design**: Fully responsive and accessible on various devices and screen sizes.

## Getting Started

### Prerequisites

- Node.js
- npm (Node Package Manager)

### Installation

-npm install

### Usage

## To run the project locally:

-npm start
This runs the app in the development mode. Open http://localhost:3000 to view it in your browser.

1. Repository Link
   https://gitlab.com/shekharchaudhary/covid-compass

2. Clone with SSH
   git@gitlab.com:shekharchaudhary/covid-compass.git

3. Clone with HTTPS
   https://gitlab.com/shekharchaudhary/covid-compass.git

### Built With

- Create React App - The web framework used
- Bootstrap - For styling and responsive design
- COVID-19 API - API for fetching the latest data

### Dependencies / Front stack
-React 
-Typescript
-React chart js
-MUI (🤫)

### Best Practices

- DRY
- Custom Hooks
- fetch api
- Functional component
- Minimal Api calls
- Util functions
- Unit test coverage

### Contact
- Shekhar Chaudhary (webdevshekhar@gmail.com)
